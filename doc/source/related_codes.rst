.. _related_codes:
.. index:: Related Codes

.. |br| raw:: html

  <br/>


Related Codes
*************

* `phonopy <https://phonopy.github.io/phonopy/>`_
* `phono3py <https://phonopy.github.io/phono3py/>`_
* `Kaldo <https://github.com/nanotheorygroup/kaldo>`_
* `shengBTE <https://bitbucket.org/sousaw/shengbte/>`_
* `almaBTE <https://almabte.bitbucket.io/>`_
* `TDEP <https://ollehellman.github.io/>`_
* `fhi-vibes <https://vibes-developers.gitlab.io/vibes/>`_
* `alamode <https://sourceforge.net/projects/alamode/>`_
* `PHONON <http://www.computingformaterials.com/>`_
* `GPUMD <https://github.com/brucefan1983/GPUMD>`_
* `CSLD <https://github.com/LLNL/csld>`_