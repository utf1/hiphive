Input/output and logging
========================

The functions and modules described below provide functionality for controlling
the verbosity of the output as well as for reading and writing force constant
matrices in different formats.


.. index::
   single: Function reference; Input/output

.. automodule:: hiphive.input_output
   :members:
   :undoc-members:
   :noindex:

.. index::
   single: Function reference; Logging

.. autofunction:: hiphive.input_output.logging_tools.set_config
   :noindex:
