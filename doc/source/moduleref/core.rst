.. index::
   single: Function reference; Core components
   single: Class reference; Core components

Core components
===============

The low-level functions and classes described here are part of the
:program:`hiPhive` core components and not intended to be used *directly*
during normal operation.

`config`
--------
.. automodule:: hiphive.core.config
   :members:
   :undoc-members:
   :noindex:

`cluster_space_builder`
-----------------------
.. automodule:: hiphive.core.cluster_space_builder
   :members:
   :undoc-members:
   :noindex:

`clusters`
----------
.. automodule:: hiphive.core.clusters
   :members:
   :undoc-members:
   :noindex:

`orbits`
--------
.. automodule:: hiphive.core.orbits
   :members:
   :undoc-members:
   :noindex:

`rotational_constraints`
------------------------
.. automodule:: hiphive.core.rotational_constraints
   :members:
   :undoc-members:
   :noindex:

`translational_constraints`
---------------------------
.. automodule:: hiphive.core.translational_constraints
   :members:
   :undoc-members:
   :noindex:

`atoms`
-------
.. automodule:: hiphive.core.atoms
   :members:
   :undoc-members:
   :noindex:

`structures`
------------
.. automodule:: hiphive.core.structures
   :members:
   :undoc-members:
   :noindex:

`structure_alignment`
---------------------
.. automodule:: hiphive.core.structure_alignment
   :members:
   :undoc-members:
   :noindex:

`tensors`
---------
.. automodule:: hiphive.core.tensors
   :members:
   :undoc-members:
   :noindex:

`utilities`
-----------
.. automodule:: hiphive.core.utilities
   :members:
   :undoc-members:
   :noindex:
